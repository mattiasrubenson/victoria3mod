# Goals of the mod collection #

* Tie diplomacy to your internal politics
* Make your internal politics more dynamic and reflective of your current political arrangement
* Incentivize the building of local submarkets before completely integrating all of your market
* Model the creation and circulation of money, further expanding the economic basis for politics and diplomacy

As it stands, the political factions of your country (mainly, the interest groups) apparently have no opinion on or influence over foreign policy. For me, everything in Victoria 3 should always come down to the underlying economic reality of your country, and the driving force for a lot of wars is indeed economic needs - whether they are for rare resources like rubber and oil, new masses of peasants to put to work in your factories, or just larger deposits of iron and coal. But the diplomacy that precedes war is not tied to the underlying economics at the moment. Tying diplomacy to the political system ensures that the economic grounding of the political system transfers over to the diplomatic system.

Talking about the political system... Its main feature right now is the enactment of different laws. Law enactment is tied to the economic situation of your country as it is represented through the interest groups and their ideologies, but it is very static and game-y. While 1.3 improved on this a lot, I still prefer a _system_ to the current situation of kinda-random-feeling events. In general, I dislike mechanics-as-events. Events are the seasoning on top of a mechanic, not a replacement.

Therefore one goal of this mod collection is to rework how law enactment works by putting in place a mechanic for negotiating with the powerful, relevant interest groups of your country in order to pass laws (and conduct foreign policy, see above). What this process looks like depends on your current distribution of power and Governance Principles. 

A secondary goal of the rework of law enactment is to try to prevent "cheesing" strategies where all law reform comes from the hands of the intelligentsia, even though they only hold a fraction of the political power in a country pretty much ruled by the landowners.

The economic basis for Victoria 3 is pretty sound, but it doesn't model one important part of the economy at all: money. Therefore, this mod also includes money.

# Complete Patch Notes # 

<details><summary>Patch notes</summary>

- [x] Lowered infrastructure from pops gained by _Urbanization_, _Urban Planning_ and _Modern Sewerage_.
- [x] Changed the Production Methods of **Government Administration** to use **Communication** and **Transportation** as input goods.
- [x] Added the good **Communication**.
- [x] Added the building **Communication**.
- [x] Added Base Production Methods to **Communications** building, consuming transportation, engines, paper and telephones, while producing **Communication**.
- [x] Added Availability Production Methods to **Communications** building, producing Infrastructure, Tax Capacity and **Communication**.
- [x] Added Ownership Production Methods to **Communications** building.
- [x] Added Roads building with a Base Production Method consuming **Grain** and producing **Transportation** and Infrastructure.
- [x] Added Governance Law Group.
- [ ] Added Central Council Law, adding authority and political strength to the IG of the head of state.
- [ ] Added Ministries Law, adding authority and political strength to ruling IGs.
- [x] Added Private Commissions Law, boosting capitalists investment pool efficiency and the political strength of Industrialists, while debuffing tax collection.
- [x] Added Independent Agencies Law, boosting bureaucracy production and decreasing authority.
- [x] Added Administrative Clergy Law, boosting Government Administration throughput, adding authority and political strength to the Devout.
- [ ] Added Production Methods to Government Administration tied to Governance Law Group, replacing Secularism Production Methods.
- [x] Added Measures Law Group.
- [x] Added Secret Landlord Measures Law, boosting the political strength of Landowners (and the land tax rate?).
- [x] Added Public Burgher Measures Law, boosting Trade Center throughput and political strength of the Petite bourgeoisie.
- [x] Added National Measures Law, boosting Government Administration throughput.
- [x] Added Universal Measures Law, boosting Government Administration throughput, trade route competitiveness and trade route volume.
- [x] Added Monetary Policy Law Group.
- [x] Added Gold Standard Law
- [x] Added Fractional Reserve Banking Law
- [x] Added Fiat Currency Law
- [x] Added Free Banking Law
- [x] Added Abolished Money Law
- [x] Added **Banks** building.
- [x] Removed the GDP multiplier to minting.
- [x] Added Base Production Methods to Banks, tied to the Monetary Policy Law Group.
- [x] Added Ownership Production Methods to Banks.
- [x] Made Gold a tradeable good.
- [x] Added Speculation good.
- [x] Added price inflation tracker script value `price_index_var`.
- [x] Added rate of profit tracker script value `profit_index_var`.
- [x] Added mass of profit tracker script value `profit_mass_index_var`.
- [x] Added growth of investment pool as share of investment pool script value `investment_pool_delta_percent`.
- [x] Added underinvestment journal entry for too much relative growth of investment pool.
- [ ] Added inflation event for too much gold in the market under Gold Standard.
- [ ] Added inflation event for too much currency relative to profits.
- [ ] Added infation event for too large price increases.
- [ ] Added financial crisis events.
- [ ] Added overproduction crisis journal entry.
- [ ] Added hyperinflation crisis journal entry.
- [ ] Added "political plays" preceding law changes.
- [ ] Added "political plays" preceding diplomatic plays.
- [ ] Added attitudes towards foreign policy to Interest Groups and Ideologies
- [ ] Added attitudes towards Measures Law Group to Interest Groups and Ideologies
- [ ] Added attitudes towards Governance Law Group to Interest Groups and Ideologies
- [ ] Added attitudes towards Monetary Policy Law Group to Interest Groups and Ideologies
- [ ] Added Political Movements for foreign policy.
- [ ] Made financial crises spread from country to country.
- [ ] Add production of infrastructure/transportation to Canals.
- [x] Added innovation spread output and university throughput multiplier to Urban Centers, representing the increased innovation from urbanization.
- [x] Added innovation spread output and university throughput multiplier to Trade Centers, representing the increased innovation from international trade.
- [x] Changed popneed_communication to use the new good **Communication**.
- [x] Added popneed_gold to wealthy pops, to enable them to buy gold (representing hoarding of wealth) and speculation.
- [x] Fixed services as input good in banks.
- [ ] Add stances towards measure laws to ideologies.
- [ ] Add stances towards administration governance laws to ideologies.

<details><summary>Added starting buildings (Communication, Banks, Roads) to countries</summary>

- [x] Sweden

</details>

- [ ] Write python script for creating events (i.e. prompts for title, description, etc, creation of localization file and event file) [add common financial crisis effects/triggers as options?]

Possible ideas:
- [ ] Debuff large scale Construction Sectors
- [ ] Calculate distance between states and make stats require more infrastructure the further they are from your Market Capital.
- [ ] Condition the convoy cost of shipping lanes on ports along the way

The dynamic we're seeking is that integrating your market requires large amounts of infrastructure, which requires large amounts of capital investment, which means that you can either (1) attract foreign investors, (2) encourage rampant financial speculation, (3) secure foreign loans, (4) enter a market with excess capital [cheap gold?] or (5) have large gold reserves. This ties into the decentralized nations mechanic, where they're very low on capital, have enormous interest rates and thus are kept economically decentralized and backwaters.

 An idea is to make banks consume a good whose price is tied to the rate of profit, so that a negative rate of profit makes profitability of capital low.

 Is it possible to get clout on a state by state basis? That could be basis for law enactment either in a council republic(?) or in a federal country.
 If possible, one could create localized effects of Igs. If landowners are strong in a state and pissed off, tax income from agriculture could fall

Could we have an interest rate mechanic where the state has an interest rate on government bonds, having to compete with private investment options etc?

Private construction sectors?

</details>

# Politics, Diplomacy and Interest Groups #

## Rework of Law Enactment ##

Enacting a new law now functions somewhat alike to diplomatic plays. You click a button to launch the play and relevant actors can pick sides, being swayed by the initiator of the play. But there's no war at the end of this tunnel - if you fail to gather support for your law enactment, the play will simply fizzle out.

What the play looks like depends on your curren **Governance Principles** law.

### Monarchy ###

Under **Monarchy**, the only relevant political actor is the monarch's Interest Group.

### Presidential Republic ###

In a **Presidential Republic**, support must be gathered from the ruling Interest Group, i.e. the Interest Group that the Head of State belongs to.

The ruling IG/Party will have decreasing approval as long as you're not enacting any laws they support.

### Parliamentary Republic ###

In a **Parliamentary Republic**, enacting a law requires support from a majority of the clout in your country, regardless of which Interest Groups are in government.

When forming a government, you can select an agenda - i.e. laws to enact. Each IG/Party will have preferences for these laws determining an overall willingness to join a ruling coalition. After forming your government, each IG will start a government petition based on your agenda.

### Theocracy ###

Who holds power in a **Theocracy**? The Devout, of course.  

### Council Republic ###

???

### What does the process look like? ###

What does it mean to gather support for enactment of a law from an Interest Group? 

Well, first and foremost, the Interest Groups that already endorse the law are of course in favor of enacting it from the start. Interest Groups that are neutral or oppose the law are open to a deal. If you agree to promise them something they want, they can support the enactment of this particular law. What do they want? That is up to them! All swayable Interest Groups will present you with a _possible_ law change they desire, and if you agree they will support your cause. If you don't, they'll oppose it.

After having promised a certain law, you'll have X years to fulfill this promise. When the time runs out, the Interest Group will be radicalized (and leave the government, if they had a place in it). If you succeed in keeping your promise, the Interest Group will become happier.

### Details for implementation ###

When you press the enact law button, chances drop to 0% and a journal entry is activated. In this journal entry there are scripted buttons for all the swayable Interest Groups, informing you of what they want in exchange for their support. When you manage to secure the relevant support, countdowns start for the law changes you've promised.

## Interest Group support for Diplomatic Plays ##

Diplomatic Plays now require you to go through something very close to the system for "political plays" described above. Interest Groups have vested interests in your foreign policy and expect to influence not just domestic politics, but international politics too.



# Rework of Infrastructure #

The governments of Victoria 3 are too good. Unrealistically and, crucially, unfunnily good. They hover very close to 100% tax capacity and 100% infrastructure in all their incorporated land. This removes all struggle from bringing your realm towards a truly centralized, integrated economic behemoth. There's no need to plan for local submarkets and no need to ponder whether you actually have the capacity to tax the residents of a certain state. Is the number red --> Click the button --> Live happily ever after.

With this mod, that's all about to change...

## Infrastructure ##

First of all, we're making it a lot harder to truly integrate your market. Achievements like the transcontinental railway actually integrating all of the US-american market, or the Siberian Railway connecting resources in the far eastern reaches of Russia to the industrial centers craving them in the west, are something to aspire to.

The basic move underlying this is to vastly decrease the infrastructure gained from pops. The nerf applies most heavily to Urbanization, then to Urban Planning and Modern Sewerage. This means that you're in a large deficit of infrastructure in the early game, until you research railways and can start to manage this, and late game technology then helps you out immensely with large scale urbanization providing benefits to infrastructure production. As a side effect, the infrastructure provided by river state traits becomes much more important early game.

So, to begin with, we're in a deficit of infrastructure, making sure that you're incentivized to build local supply chains.

## Tax Capacity ##

Second of all, we're making it a lot harder to max out your tax capacity, even in countries that are not Qing, Japan, the Sikh Empire or similarly densely populated countries. This is achieved by changing the production methods of the building Government Administration. In the vanilla game, the input goods of your Government Administration is simply Paper and then in the late game also Telephones. In this mod, on the other hand, your Government Administration consumes Transportation - sometimes a tax official might actually need to visit a local estate to ascertain the amounts of tax to claim from it - and Communication.

Your Government Administration thus needs a solid economic underpinning in the provision of these goods (and remember, you'll most probably need to produce them locally, as the price increase from low market access might otherwise make it unfeasible). Early in the game, this should prove quite the hurdle to overcome if you want 100% tax capacity everywhere.

| Simple Organization | Filing Cabinets | Standardized Filing System | Telephone Switchboards |
| ------ | ------ | ------ | ------ |
| - Transportation | - Transportation | - Transportation | - Transportation |
| - Communication | - Communication | - Communication | - Communication |
|  | - Paper | - Paper | - Paper |
|  |  |  | - Telephones |
| + Bureaucracy | + Bureaucracy | + Bureaucracy | + Bureaucracy |
| + Tax Capacity | + Tax Capacity | + Tax Capacity | + Tax Capacity |

## Communication ##

Communication is produced in the very creatively named Communications building. The building has three production methods: Base, Availability and Ownership.

### Base Production Method ###

| Stagecoach Post | Mail on Rail | Telephone Network |
| ------ | ------ | ------ |
| + Communication | + Communication | + Communication |
| - Transportation | - Transportation |  |
| - Paper | - Paper |  |
|  | - Engines | - Telephones |

The Base Production Method models the inputs to your communication production. Concretely, do your letters travel using coaches on the roads of your country, on railroads or fax machines.

Both Mail on Rail and Telephones disallows the Ownership production method Privately Owned, as it's intended to model things like the Thurn und Taxis system.

### Availability Production Method ###

| State Only | Commercialized | Penny Postage |
| ------ | ------ | ------ |
| + Infrastructure | + Infrastructure |  |
| + Tax Capacity |  |  |
| + Communication | + Communication | + Communication |

Only allowing the state to use your communications network drastically decreases the production of Communication, but it also provides a flat bonus to tax capacity in the state. On the other hand, if you want to provide a widely available communications network for your pops needs, you'll probably want to switch to the Penny Postage method, which vastly outproduces the others in terms of Communication output.

### Ownership Production Method ###

The standard ownership variants apply: privately owned (aristocrats), publicly traded (capitalists), government run and worker cooperative.

Publicly Traded is unlocked by Mutual Funds and Government Run by Centralization.

## Transportation ##

Transportation is not a new good, but since we've made Government Administration depend on it, there's a new way to provide it: good old roads. The Roads building allows you to produce infrastructure and Transportation before you've researched Railways. 

Roads are simple and ineffective, but they're your only hope early on in the game.

## Governance laws ##

We've introduced a new Law Group: Governance. This law regulates how your Government Administration actually works. It is tied to the Production Method for Government Administration that was previously called **Secularism**, but is now called **Governance**.

| Central Council | Ministries | Private Commissions | Independent Agencies | Administrative Clergy |
| ------ | ------ | ------ | ------ | ------ |
| + 10 Authority | + 5 Authority | +10% Capitalists Investment Pool Efficiency | + 10 Bureaucracy | + 10 Authority |
| + 5 % Political Strength for the IG of the head of state | + 5% Political Strength from belonging to ruling IG's | -10% Tax Collection Efficiency (OR? - dividends tax rate?) | - 5 Authority | + 5% Government Administration throughput |
|  |  |  |  | + 15% Devout political strength |

If you want authoritarian rule, go with the Central Council, placing all power in the hands of the head of state. The slightly weaker version of this law is the Ministries law, giving power to all ruling Interest Groups, who are sure to appoint loyal ministers.

The Private Commissions represents a privatized government, where wealthy individuals get to direct local administrations (some might call it corrupt, others simply _efficient_). Private Commissions will boost your investment pool, as local capitalists find the government more cooperative, but it will also decrease tax collection, for the same reason.

Independent Agencies represent a form of governance where ministers can't politically direct their agencies, who have autonomy to follow law and ethics above orders. This makes your bureaucracy stronger, but you'll lose authority.

And lastly, Administrative Clergy lets the church handle such pesky matters as the actual collection of taxes and such. They'll make your administration cheaper, but also empower the Devout IG.

As previously mentioned, these laws direct which Production Method is active in your Government Administration buildings.

## Measure laws ##

One large obstacle to efficient administration was the presence of a large number of quite different measures inside a single country. It took the rulers of France about 1000 years to achieve a single, national measure after Charlemagne pronounced it a goal of his. Most countries had a similarly harsh road to nationalized measures, requiring either conquest (often by the French), revolution (as in most countries in the former Eastern Bloc) or market integration (like in Sweden).

The road to metrication (should you follow it) is represented by another new Law Group: Measures.

| Secret Landlord Measures | Public Burgher Measures | National Measures | Universal Measures |
| ------ | ------ | ------ | ------ |
| + 15% Landowners political strength | + 15% Petite bourgeoisie political strength | + 5% Government Administration Throughput | + 5% Government Administratin Throughput |
| (+ land tax rate?) | + 5% Trade Center Throughput |  | + 25% Trade Route Competitiveness |
| | (+ 5% Communications Throughput?) |  | + 10 % Trade Route Volume |

The Secrete Landlord Measures represent the situation that prevailed in large parts of Europe at the start of the 19th century. The physical measure for determining for example a bushel of grain was kept in secret at the aristocrat's estate and only his servants were allowed to use it. This made sure that the aristocrat could use one set of measures for collecting taxes from peasants, and another for determining his dues.

Public Burgher Measures are those publicly available measures that were kept in many towns more connected to (international) trade. Those were often chained to the wall in a central market or important building and were used to determine conflicts between people about the measure of something.

National Measures represent the nationally consistent set of measures that everyone was forced to use for adjudication. Universal Measures on the other hand represents the goal of a state to use whatever measure is most commonly used internationally, representing the growth of the meter as an international measure.

# Money and Banks #

## Monetary Policy ##

We've implemented four models of money creation:
* Turning gold into currency (_minting, in its non-game meaning_) under the Gold standard
* Private creation of government denominated currency backed by assets (fractional reserve banking)
* Unregulated private creation of currency (free banking)
* Government creation of unbacked currency (fiat currency)

... as well as a model of abolished money. These models are represented by a new law group: **Monetary Policy**.

| Gold Standard | Fractional Reserve Banking | Fiat Currency | Free Banking | Abolished Money |
| ------ | ------ | ------ | ------ | ------ |

These laws regulate money creation and circulation in your economy. 

## Banks ##

All money creation in the mod is done through the new building Banks, except for the base minting of £500 that all countries have. Minting from GDP has been nullified completely. Banks have two production methods. The first method is the banks' base production method. This method is a little special, as it's only possible to change through changing the monetary policy of your country, and is consequently always the same for all your Banks.

### Creation of Money ###

| Gold Standard | Fractional Reserves | Fiat Currency | Free Banking |
| ------ | ------ | ------ | ------ |
| - Gold | - Gold | - | - |
| - Services | - Services | - Services | - Services |
| - Communication | - Communication | - Communication | - Communication |
| + Minting | + Minting | + Minting | - |
|  | + Investment Pool efficiency | + Investment Pool efficiency | + Investment Pool efficiency |
| + Speculation | + Speculation | + Speculation | + Speculation |
|  | + Banks throughput | + Banks throughput | + Banks throughput |

Under the gold standard, Banks buy Gold and turn it into minting. This represents the seigniorage the state got from the minting of coins that banks could then lend to other economic actors, and later from the interest-bearing government-guaranteed bonds (backed by gold) that replaced actual coins in many commercial banks.

Under Fractional Reserve Banking, Banks still need to buy gold, but their production is multiplied, since they only need to hold a fraction of the value of their liabilities in gold. This means that Banks under Fractional Reserve Banking also produce a new good called Speculation. Speculation is a non-tradeable good with no buyers. But the more Speculation sell orders in your market, the larger the risk for financial crises of overspeculation.

Fiat Currency means that the Banks no longer have to buy any gold at all. This makes it an easy way to achieve the liquidity you need, for example when in a long, expensive war, but it also makes it very easy to flood your market with Speculation, ensuring a coming crash.

Under Free Banking, Banks no longer issue government denominated currency, but instead their own. This means that the state no longer receives any seigniorage at all, but the fact that Banks compete with each other for creditors means that Banks under Free Banking produce lower amounts of Speculation than they would do under Fiat Currency.

### Ownership of Banks ###

Banks also have an ownership Production Method. They can be owned by the petite bourgeoisie in the form of shopkeepers, representing local family-owned banks; by aristocrats, representing the hereditary banking empires that dominated the early renaissance; by capitalists, representing commercial banks; by the government, representing nationalized banks; and by the workers of the building, representing cooperative banks.

Different ownership means slight differences in lending policies. Smaller banks are more careful, larger commercial banks can take on more risk (produce more speculation, but also boosts capitalists investment pool efficiency), banks run by aristocrats are more likely to lend money to other aristocrats than nouveau riche upstarts (boosts aristocrats investment pool efficiency), et cetera.

### Inflation ###

Let's start this section by acknowledging that the issue of inflation - what causes it, what are its effects, how to combat it and even _what it is_ - is highly debated. As debated as the issue of what money is, how it is created, et cetera. In this mod we try to simplify and abstract to achieve a model that is passable for reality and fun to play with.

As far as inflation is concerned, we allow it to appear under three guises:

* If you're under the gold standard and produce/import too much gold, its price will fall, triggering inflation events.
* If you're producing too much currency relative to current profitability trends, inflation events will trigger due to overproduction of currency relative to economic activity.
* If your market has too large general price increases, inflation events will trigger due to high a high price index.

<details><summary>Technical notes on inflation mechanics</summary>

The script value `price_index_var` tracks your current price levels by iterating through all goods in your market and averaging over their priciness (i.e. their divergance from base price/cost). 

The script value `profit_index_var` is calculated by iterating through all non-government buildings in your country and averaging their weekly profit divided by their level. 

The script value `profit_mass_var` is calculated by iterating through all non-government buildings in your country and adding up their weekly profit.

</details>

### Financial Crises ###

Inflation events are just one kind of financial crisis that can be experienced in this mod. There are many events that are triggered by overinvestment in certain sectors, too great a divergence between Speculation growth and GDP growth, et cetera.

### Economic Crises ###

While financial crises are short-term events, an economic crisis is larger in scale. We have a few different possible economic crises in this mod, with different characteristics.

#### Crises of overproduction ####

A crisis of overproduction occurs when wealth is hoarded instead of invested, following price decreases and falling profitability.

These events trigger when
* Capitalists' wealth > X
* The rate of profit has been falling for Y months
* The mass of profit has been non-positive for Z months.

#### Crises of hyperinflation ####

Aside from the short-term inflation events mentioned above, we also simulate crises of hyperinflation, such as the ones that occured in the Soviet Union following the October Revolution and in Weimar Germany in the 1920's.
