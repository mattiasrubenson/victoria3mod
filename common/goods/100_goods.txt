gold = {
	texture = "gfx/interface/icons/goods_icons/gold.dds"
	cost = 100
	category = luxury
	tradeable = yes
	fixed_price = no
	prestige_factor = 7
}

communication = {
	texture = "gfx/interface/icons/goods_icons/transportation.dds"
	cost = 30
	category = staple
	tradeable = no

	prestige_factor = 4
	
	consumption_tax_cost = 200
}

speculation = {
	texture = "gfx/interface/icons/goods_icons/transportation.dds"

	cost = 100
	category = luxury
	tradeable = no
	fixed_price = yes
}

profit = {
	texture = "gfx/interface/icons/goods_icons/gold.dds"

	cost = 100
	category = luxury
	tradeable = no
	fixed_price = no
}

