law_gold_standard = {
	group = lawgroup_monetary_system

	progressiveness = -5

	disallowing_laws = {
	}

	modifier = {
		building_gold_mine_throughput_mult = 0.1
		building_gold_fields_throughput_mult = 0.1
		state_building_bank_max_level_add = 1
	}

	pop_support = {
		value = 0
		add = {
			desc = "POP_ARISTOCRATS"
			if = {
				limit = {
					is_pop_type = aristocrats
				}
				value = 0.33
			}
		}
	}

	pop_support = {
		value = 0
		add = {
			desc = "POP_FARMERS"
			if = {
				limit = {
					is_pop_type = farmers
				}
				value = 0.5
			}
		}
	}
}

law_fractional_reserves = {
	group = lawgroup_monetary_system

	progressiveness = 5

	disallowing_laws = {
	}

	modifier = {
		#+ capitalists inv. efficiency
		state_building_bank_max_level_add = 1
	}

	pop_support = {
		value = 0
		add = {
			desc = "POP_CAPITALISTS"
			if = {
				limit = {
					is_pop_type = capitalists
				}
				value = 0.5
			}
		}
	}

	pop_support = {
		value = 0
		add = {
			desc = "POP_LABORERS"
			if = {
				limit = {
					is_pop_type = laborers
				}
				value = 0.2
			}
		}
	}
}

law_fiat_currency = {
	group = lawgroup_monetary_system

	progressiveness = 15

	disallowing_laws = {
	}

	unlocking_technologies = {
		central_banking
	}

	modifier = {
		building_gold_mine_throughput_mult = -0.8
		building_gold_fields_throughput_mult = -0.8
		state_building_bank_max_level_add = 1
		#+ capitalists inv. efficiency
	}

	pop_support = {
		value = 0
		add = {
			desc = "POP_CAPITALISTS"
			if = {
				limit = {
					is_pop_type = capitalists
				}
				value = 0.33
			}
		}
	}
}

law_free_banking = {
	group = lawgroup_monetary_system

	progressiveness = 25

	disallowing_laws = {
	}

	unlocking_technologies = {
		mutual_funds
	}

	unlocking_laws = {
		law_laissez_faire			
	}

	modifier = {
		country_minting_mult = -1
		state_building_bank_max_level_add = 1
		#+ capitalists inv. efficency
	}

	pop_support = {
		value = 0
		add = {
			desc = "POP_CAPITALISTS"
			if = {
				limit = {
					is_pop_type = capitalists
				}
				value = 0.33
			}
		}
	}
}

law_abolished_money = {
	group = lawgroup_monetary_system

	progressiveness = 15

	unlocking_laws = {
		law_anarchy
	}

	modifier = {
		country_disable_investment_pool = yes
		interest_group_ig_industrialists_pol_str_mult = -0.5
	}

	on_activate = {
		every_scope_state = {
			if = {
				limit = {
					has_building = building_bank
				}
				remove_building = building_bank
			}
		}
	}

	pop_support = {
		value = 0
		add = {
			desc = "POP_FARMERS"
			if = {
				limit = {
					is_pop_type = farmers
				}
				value = 0.2
			}
		}
	}

	pop_support = {
		value = 0
		add = {
			desc = "POP_LABORERS"
			if = {
				limit = {
					is_pop_type = laborers
				}
				value = 0.2
			}
		}
	}

	ai_will_do = {
		always = no
	}
}