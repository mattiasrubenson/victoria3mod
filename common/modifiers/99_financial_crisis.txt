financial_crisis_effects_on_state = {
	state_capitalists_investment_pool_efficiency_mult = -0.25		
}

financial_crisis_effects_on_state_farmers = {
	state_farmers_investment_pool_efficiency_mult = -0.25		
}

inflation_effects_on_state = {
	building_gold_mine_throughput_mult = -0.5
	building_gold_fields_throughput_mult = -0.5
	building_bank_throughput_mult = -0.5
}

bailed_out_bank_effects_on_building = {
	building_output_speculation_add = 5
}

# This removes all minting from GDP
country_gdp = {
    icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_positive.dds
    country_minting_add = 0
}

# Separate tax modifiers
dividends_tax_modifier = {
	tax_dividends_mult = -0.01
}

underinvestment_timeout = {
	interest_group_ig_industrialists_approval_add = -15
}