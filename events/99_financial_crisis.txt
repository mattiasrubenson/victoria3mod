namespace = financial_crisis_events

financial_crisis_events.1 = { # Economic Crisis averted
	type = country_event
	title = financial_crisis_events.1.t
	desc = financial_crisis_events.1.d
	flavor = financial_crisis_events.1.f

	cooldown = { days = 1 }

	duration = 10

	event_image = {
		video = "asia_dead_cattle_poor_harvest"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_protest.dds"

	option = {
		name = financial_crisis_events.1.a
	}
}

financial_crisis_events.2 = { # Bank run version 1
	type = country_event
	placement = root
	title = financial_crisis_events.2.t
	desc = financial_crisis_events.2.d
	flavor = financial_crisis_events.2.f

	cooldown = { days = 1 }

	duration = 3

	event_image = {
		video = "asia_dead_cattle_poor_harvest"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_protest.dds"

	trigger = {
		market_capital.market = {
			mg:speculation = {
				market_goods_production > 100
			}
		}
		any_scope_state = {
			any_scope_building = {
				is_building_type = building_bank
				OR = {
					level > 5
					has_active_production_method = pm_bank_ownership_commercialized
				}
			}
		}
	}
	
	immediate = {
		random_scope_state = {
			limit = {
				any_scope_building = {
					is_building_type = building_bank
					OR = {
						level > 5
						has_active_production_method = pm_bank_ownership_commercialized
					}
				}
			}
			save_scope_as = financial_crisis_state
		}
	}

	option = {
		name = financial_crisis_events.2.a # Bail out the bank
		default_option = yes
		add_treasury = money_amount_multiplier_large
		scope:financial_crisis_state = {
			add_radicals_in_state = {
				strata = middle
				value = 0.4
			}
		}
	}

	option = {
		name = financial_crisis_events.2.b # Let the bank collapse
		scope:financial_crisis_state = {
			add_radicals_in_state = {
				pop_type = capitalists
				value = 0.5
			}
			add_modifier = {
				name = financial_crisis_effects_on_state
				months = normal_modifier_time
			}
			remove_building = building_bank
		}
	}
}

financial_crisis_events.3 = { # Inflation event
	type = country_event
	title = financial_crisis_events.3.t
	desc = financial_crisis_events.3.d
	flavor = financial_crisis_events.3.f

	cooldown = {
		months = 1
	}

	trigger = {
		NOT = {	has_law = law_type:law_abolished_money }
		market_capital.market = {
			mg:gold = {
				market_goods_production > 1000
			}
		}
	}

	duration = 3


	immediate = {
		random_scope_state = {
			limit = { 
				OR = {
					has_building = building_gold_mine
					has_building = building_gold_fields
					has_building = building_bank
				}
			}
			save_scope_as = inflation_state
		}
	}

	option = { # Go to the gold standard
		name = financial_crisis_events.3.a
		trigger = {
			OR = {
				has_law = law_type:law_fractional_reserves
				has_law = law_type:law_fiat_currency
				has_law = law_type:law_free_banking
			}
		}
		activate_law = law_type:law_gold_standard
	}

	option = { # Debuff gold throughput to limit gold supply
		name = financial_crisis_events.3.b
		trigger = {
			has_law = law_type:law_gold_standard
		}
		add_modifier = {
			name = inflation_effects_on_state
			months = normal_modifier_time
		}
	}
}

financial_crisis_events.4 = { # Worries of War
	type = country_event
	placement = root
	title = financial_crisis_events.4.t
	desc = financial_crisis_events.4.d
	flavor = financial_crisis_events.4.f

	cooldown = { days = 1 }

	duration = 3

	event_image = {
		video = "asia_dead_cattle_poor_harvest"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_protest.dds"

	trigger = {
		root = {
			is_at_war = yes
		}
		any_scope_state = {
			any_scope_building = {
				is_building_type = building_bank
				OR = {
					has_active_production_method = pm_bank_ownership_family
					level < 3
				}
			}
		}
	}
	
	immediate = {
		random_scope_state = {
			limit = {
				any_scope_building = {
					is_building_type = building_bank
					OR = {
						has_active_production_method = pm_bank_ownership_family
						level < 3
					}
				}
			}
			save_scope_as = financial_crisis_state
		}
	}

	option = {
		name = financial_crisis_events.4.a # Bail out the bank
		default_option = yes
		add_treasury = -100000
		scope:financial_crisis_state = {
			add_radicals_in_state = {
				strata = middle
				value = 0.2
			}
		}
	}

	option = {
		name = financial_crisis_events.4.b # Let the bank collapse
		scope:financial_crisis_state = {
			if = {
				limit = {
					any_scope_building = {
						is_building_type = building_bank
						has_active_production_method = pm_bank_ownership_family
					}
				}
				add_radicals_in_state = {
					pop_type = farmers
					value = 0.5
				}
				add_modifier = {
					name = financial_crisis_effects_on_state_farmers
					months = normal_modifier_time
				}
			}
			else_if = {
				limit = {
					any_scope_building = {
						is_building_type = building_bank
						NOT = { has_active_production_method = pm_bank_ownership_family }
						level < 3
					}
				}
				add_radicals_in_state = {
					pop_type = capitalists
					value = 0.2
				}
				add_modifier = {
					name = financial_crisis_effects_on_state
					months = normal_modifier_time
				}
			}
			remove_building = building_bank
		}
	}
}

financial_crisis_events.5 = { # Suez canal built
	type = country_event
	placement = root
	title = financial_crisis_events.5.t
	desc = financial_crisis_events.5.d
	flavor = financial_crisis_events.5.f

	option = {
		name = financial_crisis_events.5.a
	}

}